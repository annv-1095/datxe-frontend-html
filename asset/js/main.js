$(document).ready(function () {
  $('.menu-mb-open').click(function () {
    $("body").addClass("open-menu-mb");
  });
  $('.menu-mb-close').click(function () {
    $("body").removeClass("open-menu-mb");
  });

  $('.gotoTop').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
  });

  $('.news-slide-carousel').owlCarousel({
    items: 1,
    loop: true,
    pagination: true,
  })
  $('.partner-carousel').owlCarousel({
    items: 1,
    loop: true,
    margin: 10,
    nav: true,
    navText: [
      '<div class="nav-btn"><i class="icon icon-prev" aria-hidden="true"></i></div>',
      '<div class="nav-btn"><i class="icon icon-next" aria-hidden="true"></i></div>'
    ],
    navContainer: '.home-partner-navs',
    onInitialized: counter,
    onTranslated: counter
  })

  $('.nd-redlated-carousel').owlCarousel({
    items: 3,
    loop: true,
    margin: 20,
    nav: true,
    navText: [
      '<i class="fa fa-angle-left" aria-hidden="true"></i>',
      '<i class="fa fa-angle-right" aria-hidden="true"></i>'
    ],
    navContainer: '.nd-related-navs',
    onInitialized: counter,
    onTranslated: counter,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
  })

  $('.carousel-aff-price').owlCarousel({
    center: true,
    loop: true,
    items: 1,
    nav: true,
    navText: [
      '<div class="ap-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
      '<div class="ap-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></div>'
    ],
  })

  function counter(event) {
    var element = event.target;
    var items = event.item.count;
    var item = event.item.index - 1;

    if (item > items) {
      item = item - items
    }
    $('.home-partner-count').html(item + "/" + items)
  }
});
$(window).scroll(function () {
  if ($(window).scrollTop() >= 100) {
    $('.menu-top-main').addClass('sticky-header');
  }
  else {
    $('.menu-top-main').removeClass('sticky-header');
  }
});